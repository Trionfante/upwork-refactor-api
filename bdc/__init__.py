import os
from flask import Flask
from flask_cors import CORS
from flask_restplus import Api
from flask_sqlalchemy import SQLAlchemy
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)   # to enable SwaggerUI
CORS(app)

try:
    config = os.environ['API_SETTINGS']
except KeyError:
    config = 'config_dev'
app.config.from_object(config)

db = SQLAlchemy(app)
api = Api(app, version='1.0', title='RAMS API')

import bdc.views
