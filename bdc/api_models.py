from flask_restplus import fields

from bdc import api


def make_element(element, location, unit, area_or_quantity=None):
    return {
        'element': element,
        'location': location,
        'area_or_quantity': area_or_quantity,
        'unit': unit,
        'cs1': 0,
        'cs2': 0,
        'cs3': 0,
        'cs4': 0,
        'damaged': '',
        'comment': ''
    }


bridge_model = api.model('Bridge', {
    "bridge_id": fields.String(description='Unique bridge ID', optional=False),
    "bridge_name": fields.String,
    "zone": fields.String,
    "inspector": fields.String,
    "inspection_date": fields.DateTime(optional=False),
    "source_of_info": fields.String,
    "road_name": fields.String,
    "total_length": fields.Float,
    "deck_width": fields.Float,
    "latitude": fields.Float,
    "longitude": fields.Float,
    "principal_feature": fields.String,
    "secondary_feature": fields.String,
    "feature_crossed": fields.String,
    "num_of_spans": fields.Integer,
    "span_length_1": fields.Float,
    "span_length_2": fields.Float,
    "span_length_3": fields.Float,
    "span_length_4": fields.Float,
    "span_length_5": fields.Float,
    "span_length_6": fields.Float,
    "span_length_7": fields.Float,
    "sidewalk_width_left": fields.Float,
    "sidewalk_width_right": fields.Float,
    "carriageway_width": fields.Float,
    "approachway_width": fields.Float,
    "clearances_vertical": fields.Float,
    "clearances_freeboard": fields.Float,
    "design_firm": fields.String,
    "contractor": fields.String,
    "construction_year": fields.Integer,
    "major_rehabilitation_year": fields.Integer,
    "design_live_road": fields.String,
    "design_code": fields.String,
    "super_loads": fields.String,
    "weight_load_restriction": fields.String,
    "skew_angle": fields.String,
    "design_return_period": fields.String,
    "design_flood_discharge": fields.String,
    "design_freeboard": fields.String,
    "load_management": fields.String,
    "load_rating": fields.String,
    "seismic_load_rating": fields.String
})

inventory_model = api.model('Inventory', {
    "inspection_date": fields.DateTime(optional=False),
    "type_of_bridge": fields.String(optional=False),
    "structural_form": fields.String,
    "deck_type": fields.String,
    "deck_material": fields.String,
    "edge_beam": fields.String,
    "main_girder_type": fields.String,
    "main_girder_material": fields.String,
    "cross_beam_type": fields.String,
    "cross_beam_matieral": fields.String,
    "construction_joints": fields.String,
    "abutements_type": fields.String,
    "abutements_material": fields.String,
    "abutement_cap": fields.String,
    "wing_walls_type": fields.String,
    "wing_walls_material": fields.String,
    "pier": fields.String,
    "pier_material": fields.String,
    "pier_cap": fields.String,
    "bearing1": fields.String,
    "bearing2": fields.String,
    "bearing_seat": fields.String,
    "expansion_joints": fields.String,
    "outlet_pipes": fields.String,
    "downsprout_pipes": fields.String,
    "deck_drains": fields.String,
    "pipe": fields.String,
    "run_on_slab_cover": fields.String,
    "run_on_slab_kerbs": fields.String,
    "run_on_slab_vehicle_restraint_system": fields.String,
    "run_on_slab_drainage_gully": fields.String,
    "pavement_road": fields.String,
    "pavement_sidewalk": fields.String,
    "barrier_vehicle_restraint_system": fields.String,
    "barrier_hand_rails": fields.String,
    "bridge_sign": fields.Boolean,
    "reflectors": fields.Boolean,
    "electric_cables": fields.Boolean,
    "telecom_cables": fields.Boolean,
    "sewers": fields.Boolean,
    "water_mains": fields.Boolean,
    "stormwater_pipes": fields.Boolean,
    "gas": fields.Boolean,
    "unknown": fields.Boolean,
    "riverbed_protection": fields.String,
    "riverbed_slopes": fields.String,
    "foundation": fields.String,
    "comments": fields.String,
    "design_drawings_available": fields.Boolean,
    "as_built_drawings_available": fields.Boolean,
    "microfilm_available": fields.Boolean,
    "washed_out_before": fields.Boolean
})

condition_model = api.model('Condition', {
    "inspection_date": fields.DateTime(optional=False),
    "element": fields.String,
    "location": fields.String,
    "area_or_quantity": fields.Integer,
    "unit": fields.String,
    "cs1": fields.Integer,
    "cs2": fields.Integer,
    "cs3": fields.Integer,
    "cs4": fields.Integer,
    "damaged": fields.Boolean,
    "comment": fields.String
})

iri_roadclass_paved_model = api.model('IriRoadclassPaved', {
    "Roadclass": fields.String,
    "Average IRI (mm/m)": fields.Float
})

iri_roadclass_unpaved_model = api.model('IriRoadclassPaved', {
    "Roadclass": fields.String,
    "Average IRI (mm/m)": fields.Float
})

iri_zone_paved_model = api.model('IriZonePaved', {
    "Zone": fields.String,
    "Average IRI (mm/m)": fields.Float
})

iri_zone_unpaved_model = api.model('IriZoneUnpaved', {
    "Zone": fields.String,
    "Average IRI (mm/m)": fields.Float
})

iri_zoneroadclass_paved_model = api.model('IriZoneRoadclassPaved', {
    "Zone": fields.String,
    "Roadclass": fields.String,
    "Average IRI (mm/m)": fields.Float
})

iri_zoneroadclass_unpaved_model = api.model('IriZoneRoadclassUnpaved', {
    "Zone": fields.String,
    "Roadclass": fields.String,
    "Average IRI (mm/m)": fields.Float
})

pavement_condition_roadclass_model = api.model('PavementConditionRoadlcass', {
    "Road class": fields.String,
    "Condition category": fields.Float
})

pavement_condition_zone_model = api.model('PavementConditionZone', {
    "Zone": fields.String,
    "Condition category": fields.Float
})

pavement_condition_zoneroadclass_model = api.model('PavementConditionZoneRoadclass', {
    "Zone": fields.String,
    "Road class": fields.String,
    "Condition category": fields.Float
})

cracking_model = api.model('Cracking', {
    "Roadcode": fields.String,
    "start20": fields.Integer,
    "end20": fields.Integer,
    "avg": fields.Float,
    "Condition category": fields.String
})

edge_break_model = api.model('EdgeBreak', {
    "Roadcode": fields.String,
    "start20": fields.Integer,
    "end20": fields.Integer,
    "avg": fields.Float,
    "Condition category": fields.String
})

pavement_loss_model = api.model('PavementLoss', {
    "Roadcode": fields.String,
    "start20": fields.Integer,
    "end20": fields.Integer,
    "avg": fields.Float,
    "Condition category": fields.String
})

potholes_model = api.model('Potholes', {
    "Roadcode": fields.String,
    "start20": fields.Integer,
    "end20": fields.Integer,
    "avg": fields.Float,
    "Condition category": fields.String
})

roadwise_pdi_model = api.model('RoadwisePDI', {
    "Roadcode": fields.String,
    "road_width": fields.Float,
    "road_length": fields.Float,
    "pothole_area_percentage": fields.Float,
    "cracking_area_percentage": fields.Float,
    "missing_pavement_area_percentage": fields.Float,
    "edge_break_area_percentage": fields.Float,
    "defect_percentage": fields.Float
})

road_value_model = api.model('RoadValue', {
    "Road code": fields.String,
    "Surface type": fields.String,
    "Width (m)": fields.Float,
    "Length (m)": fields.Float,
    "Average IRI (mm/m)": fields.Float,
    "Land value (ECD)": fields.Float,
    "Construction value (ECD)": fields.Float,
    "Total value (ECD)": fields.Float
})

report_roads_model = api.model('ReportRoads', {
    "Road Code": fields.String,
    "Road Name": fields.String,
    "Road Class": fields.String,
    "Road Length (m)": fields.Float
})

report_iri_model = api.model('ReportIRI', {
    "Road Code": fields.String,
    "Road Name": fields.String,
    "International Roughness Index (mm/m)": fields.Float
})

report_traffic_model = api.model('ReportTraffic', {
    "Road Code": fields.String,
    "Road Name": fields.String,
    "Start": fields.Integer,
    "End": fields.Integer,
    "Motorcycle": fields.Integer,
    "Passenger car": fields.Integer,
    "Minibus": fields.Integer,
    "Medium bus": fields.Integer,
    "Large bus": fields.Integer,
    "Light truck": fields.Integer,
    "2Axle truck": fields.Integer,
    "3Axle truck": fields.Integer,
    "4+Axle truck": fields.Integer,
    "Annual Average Daily Traffic": fields.Integer
})

report_pdi_model = api.model('ReportPDI', {
    "Road code": fields.String,
    "Road width (m)": fields.Float,
    "Road length (m)": fields.Integer,
    "Pothole area (%)": fields.Float,
    "Cracking area (%)": fields.Float,
    "Missing pavement area (%)": fields.Float,
    "Edge break area (%)": fields.Float,
    "Defect area (%)": fields.Float,
    "Weighted defect area (%)": fields.Float,
    "Condition category": fields.Float
})

report_surface_type_model = api.model('ReportSurfaceType', {
    "Road code": fields.String,
    "Asphalt concrete (%)": fields.Float,
    "Cement concrete (%)": fields.Float,
    "Surface dressing (%)": fields.Float,
    "Unpaved (%)": fields.Float,
    "Unknown (%)": fields.Float
})

dashboard_model = api.model('Dashboard', {
    "Value": fields.String,
    "Text": fields.String,
    "Sub-text": fields.String,
    "Icon": fields.String
})

bms_webgis_model = api.model('BMSWebGIS', {
    "Bridge name": fields.String,
    "Value estimate (ECD)": fields.Integer,
    "Bridge Condition Index (BCI)": fields.Float,
    "latitude": fields.Float,
    "longitude": fields.Float
})

bms_kpi_condition_model = api.model('BMSKPICondition', {
    "Condition class": fields.Integer,
    "Condition": fields.String,
    "Number of bridges": fields.Integer
})

bms_report_value_model = api.model('BMSReportValue', {
    "Bridge name": fields.String,
    "Deck area (m2)": fields.Float,
    "Bridge Condition Index": fields.Float,
    "Unit cost (ECD)": fields.Integer,
    "Value estimate (ECD)": fields.Integer,
})
