from bdc import db


class Bridge(db.Model):
    __table_args__ = {"schema": "bdb"}
    __tablename__ = 'DATA_BRIDGES'
    id = db.Column('id', db.Integer, primary_key=True)

    # inspection details
    bridge_id = db.Column('bridge_id', db.String, unique=True)
    bridge_name = db.Column('bridge_name', db.String, nullable=False)
    zone = db.Column('zone', db.String, nullable=False)
    created_at = db.Column('created_at', db.DateTime, nullable=False)
    inspector = db.Column('inspector', db.String, nullable=False)
    inspection_date = db.Column('inspection_date', db.DateTime, nullable=False)
    source_of_info = db.Column('source_of_info', db.String, nullable=False)

    # bridge location details
    road_name = db.Column('road_name', db.String, nullable=False)
    total_length = db.Column('total_length', db.Float)
    deck_width = db.Column('deck_width', db.Float)
    latitude = db.Column('latitude', db.Float)
    longitude = db.Column('longitude', db.Float)
    principal_feature = db.Column('principal_feature', db.String)
    secondary_feature = db.Column('secondary_feature', db.String)
    feature_crossed = db.Column('feature_crossed', db.String)

    # design and construction details
    num_of_spans = db.Column('num_of_spans', db.Integer)
    span_length_1 = db.Column('span_length_1', db.Float)
    span_length_2 = db.Column('span_length_2', db.Float)
    span_length_3 = db.Column('span_length_3', db.Float)
    span_length_4 = db.Column('span_length_4', db.Float)
    span_length_5 = db.Column('span_length_5', db.Float)
    span_length_6 = db.Column('span_length_6', db.Float)
    span_length_7 = db.Column('span_length_7', db.Float)
    sidewalk_width_left = db.Column('sidewalk_width_left', db.Float)
    sidewalk_width_right = db.Column('sidewalk_width_right', db.Float)
    carriageway_width = db.Column('carriageway_width', db.Float)
    approachway_width = db.Column('approachway_width', db.Float)
    clearances_vertical = db.Column('clearances_vertical', db.Float)
    clearances_freeboard = db.Column('clearances_freeboard', db.Float)
    design_firm = db.Column('design_firm', db.String)
    contractor = db.Column('contractor', db.String)
    construction_year = db.Column('construction_year', db.Integer)
    major_rehabilitation_year = db.Column(
        'major_rehabilitation_year', db.Integer)
    design_live_road = db.Column('design_live_road', db.String)
    design_code = db.Column('design_code', db.String)
    super_loads = db.Column('super_loads', db.String)
    weight_load_restriction = db.Column('weight_load_restriction', db.String)
    skew_angle = db.Column('skew_angle', db.String)
    design_return_period = db.Column('design_return_period', db.String)
    design_flood_discharge = db.Column('design_flood_discharge', db.String)
    design_freeboard = db.Column('design_freeboard', db.String)
    load_management = db.Column('load_management', db.String)
    load_rating = db.Column('load_rating', db.String)
    load_rating = db.Column('load_rating', db.String)
    seismic_load_rating = db.Column('seismic_load_rating', db.String)


class InventoryInspection(db.Model):
    __table_args__ = {"schema": "bdb"}
    __tablename__ = 'DATA_INVENTORY'
    id = db.Column('id', db.Integer, primary_key=True)

    inspection_date = db.Column('inspection_date', db.DateTime, nullable=False)
    created_at = db.Column('created_at', db.DateTime, nullable=False)
    bridge_id = db.Column('bridge_id', db.String, nullable=False)
    type_of_bridge = db.Column('type_of_bridge', db.String, nullable=False)
    structural_form = db.Column('structural_form', db.String)
    deck_type = db.Column('deck_type', db.String)
    deck_material = db.Column('deck_material', db.String)
    edge_beam = db.Column('edge_beam', db.String)
    main_girder_type = db.Column('main_girder_type', db.String)
    main_girder_material = db.Column('main_girder_material', db.String)
    cross_beam_type = db.Column('cross_beam_type', db.String)
    cross_beam_matieral = db.Column('cross_beam_matieral', db.String)
    construction_joints = db.Column('construction_joints', db.String)
    abutements_type = db.Column('abutements_type', db.String)
    abutements_material = db.Column('abutements_material', db.String)
    abutement_cap = db.Column('abutement_cap', db.String)
    wing_walls_type = db.Column('wing_walls_type', db.String)
    wing_walls_material = db.Column('wing_walls_material', db.String)
    pier = db.Column('pier', db.String)
    pier_material = db.Column('pier_material', db.String)
    pier_cap = db.Column('pier_cap', db.String)
    bearing1 = db.Column('bearing1', db.String)
    bearing2 = db.Column('bearing2', db.String)
    bearing_seat = db.Column('bearing_seat', db.String)
    expansion_joints = db.Column('expansion_joints', db.String)
    outlet_pipes = db.Column('outlet_pipes', db.String)
    downsprout_pipes = db.Column('downsprout_pipes', db.String)
    deck_drains = db.Column('deck_drains', db.String)
    pipe = db.Column('pipe', db.String)
    run_on_slab_cover = db.Column('run_on_slab_cover', db.String)
    run_on_slab_kerbs = db.Column('run_on_slab_kerbs', db.String)
    run_on_slab_vehicle_restraint_system = db.Column(
        'run_on_slab_vehicle_restraint_system', db.String)
    run_on_slab_drainage_gully = db.Column(
        'run_on_slab_drainage_gully', db.String)
    pavement_road = db.Column('pavement_road', db.String)
    pavement_sidewalk = db.Column('pavement_sidewalk', db.String)
    barrier_vehicle_restraint_system = db.Column(
        'barrier_vehicle_restraint_system', db.String)
    barrier_hand_rails = db.Column('barrier_hand_rails', db.String)
    bridge_sign = db.Column('bridge_sign', db.Boolean)
    reflectors = db.Column('reflectors', db.Boolean)
    electric_cables = db.Column('electric_cables', db.Boolean)
    telecom_cables = db.Column('telecom_cables', db.Boolean)
    sewers = db.Column('sewers', db.Boolean)
    water_mains = db.Column('water_mains', db.Boolean)
    stormwater_pipes = db.Column('stormwater_pipes', db.Boolean)
    gas = db.Column('gas', db.Boolean)
    unknown = db.Column('unknown', db.Boolean)
    riverbed_protection = db.Column('riverbed_protection', db.String)
    riverbed_slopes = db.Column('riverbed_slopes', db.String)
    foundation = db.Column('foundation', db.String)
    comments = db.Column('comments', db.String)
    design_drawings_available = db.Column(
        'design_drawings_available', db.Boolean)
    as_built_drawings_available = db.Column(
        'as_built_drawings_available', db.Boolean)
    microfilm_available = db.Column('microfilm_available', db.Boolean)
    washed_out_before = db.Column('washed_out_before', db.Boolean)


class ConditionInspection(db.Model):
    __table_args__ = {"schema": "bdb"}
    __tablename__ = 'DATA_CONDITION'
    id = db.Column('id', db.Integer, primary_key=True)

    inspection_date = db.Column('inspection_date', db.DateTime, nullable=False)
    bridge_id = db.Column('bridge_id', db.String, nullable=False)
    created_at = db.Column('created_at', db.DateTime, nullable=False)

    element = db.Column('element', db.String)
    location = db.Column('location', db.String)
    area_or_quantity = db.Column('area_or_quantity', db.Integer)
    unit = db.Column('unit', db.String)
    cs1 = db.Column('cs1', db.Integer)
    cs2 = db.Column('cs2', db.Integer)
    cs3 = db.Column('cs3', db.Integer)
    cs4 = db.Column('cs4', db.Integer)
    damaged = db.Column('damaged', db.Boolean)
    comment = db.Column('comment', db.String)
