from datetime import datetime

from flask_restplus import Resource
import pandas as pd

from bdc import db, api
from bdc.db_models import Bridge, InventoryInspection, ConditionInspection
from bdc.api_models import make_element, bridge_model, inventory_model, condition_model

from bdc.api_models import *

from bdc.db_queries import *

bdc = api.namespace('bridges', description='bridge data collection')
rms = api.namespace('rms', description='road management system operations')
bms = api.namespace('bms', description='bridge management system operations')
fwd = api.namespace('fwd', description='fwd survey planning operations')

@bdc.route('/')
class Bridges(Resource):
    @bdc.marshal_with(bridge_model, envelope='bridges')
    def get(self):
        """Get all the bridges."""
        all_bridges = Bridge.query.all()
        return all_bridges

    @bdc.expect(bridge_model)
    @bdc.response(201, 'Bridge successfully added.')
    def post(self):
        """ Add a new bridge.

        Allows to add a new bridge. The first 5 fields are mandatory.
        Pay attention to types."""
        inspection_date = datetime.strptime(api.payload['inspection_date'],
                                            '%Y-%m-%dT%H:%M:%S.%fZ')
        api.payload['inspection_date'] = inspection_date
        api.payload['created_at'] = datetime.now()
        new_bridge = Bridge(**api.payload)
        try:
            db.session.add(new_bridge)
            db.session.commit()
        except:
            db.session.rollback()
            api.abort(409, 'Bridge with this ID already exists.')

        return {'message': 'Bridge added.'}, 201


@bdc.route('/<string:bridge_id>')
class OneBridge(Resource):
    @bdc.marshal_with(bridge_model, envelope='bridge')
    @bdc.response(404, 'Bridge not found.')
    def get(self, bridge_id):
        """Get a bridge by bridge ID.

        Allows to get individual bridge's main info."""
        bridge = Bridge.query.filter_by(bridge_id=bridge_id).first()
        if bridge:
            return bridge
        else:
            api.abort(404, "Bridge {} doesn't exist".format(bridge_id))


@bdc.route('/<string:bridge_id>/inventory')
class Inventory(Resource):
    @bdc.marshal_with(inventory_model, envelope='inventory')
    @bdc.response(404, 'No inspections found.')
    def get(self, bridge_id):
        """Get inventory inspection(s) with bridge ID.

        Allows to get individual bridge's inventory inspection data."""
        insp = InventoryInspection.query.filter_by(bridge_id=bridge_id).all()
        if insp:
            return insp
        else:
            api.abort(404, "No inspections found")

    @bdc.expect(inventory_model)
    @bdc.response(201, 'Inspection successfully added.')
    def post(self, bridge_id):
        """ Add bridge inventory.

        Allows to submit bridge inventory inspection.
        The first 2 fields are mandatory.
        Pay attention to types."""
        inspection_date = datetime.strptime(api.payload['inspection_date'],
                                            '%Y-%m-%dT%H:%M:%S.%fZ')
        api.payload['inspection_date'] = inspection_date
        api.payload['bridge_id'] = bridge_id
        api.payload['created_at'] = datetime.now()
        new_inventory = InventoryInspection(**api.payload)
        db.session.add(new_inventory)
        db.session.commit()
        return {'message': 'Inspection successfully added.'}, 201

        # TODO: add checking for bridge


@bdc.route('/<string:bridge_id>/condition/elements')
class ConditionElements(Resource):
    @bdc.marshal_with(condition_model, envelope='condition')
    @bdc.response(404, 'No bridge found.')
    def get(self, bridge_id):
        """Get condition inspection input with bridge ID.

        Allows to get a list of elements to be assessed based on inventory."""
        bridge = Bridge.query.filter_by(
            bridge_id=bridge_id).first()  # TODO: check is this is working
        if bridge:
            inventory = InventoryInspection.query.filter_by(
                bridge_id=bridge_id).first()
            deck_width = bridge.deck_width
            total_length = bridge.total_length
            carriageway_width = bridge.carriageway_width
            freeboard = bridge.clearances_freeboard
            # sidewalk_width = bridge.sidewalk_width

            elements = []

            # deck
            for i in range(1, bridge.num_of_spans + 1):
                span_length = eval('bridge.span_length_' + str(i))
                element = make_element(
                    'Deck', i, 'm2', deck_width * span_length)
                elements.append(element)

            # edge beam
            elements.append(make_element('Edge beam', 1, 'm', total_length))
            elements.append(make_element('Edge beam', 2, 'm', total_length))

            # main girder TODO: add 2 beams if girder is beam
            # TODO: riverbed

            # cross beam
            if inventory.cross_beam_type != 'none':
                elements.append(make_element('Cross beam', 1, 'pcs'))
                elements.append(make_element('Cross beam', 2, 'pcs'))

            # construction joints
            if inventory.construction_joints != 'none':
                elements.append(make_element('Construction joint', 1, 'pcs'))

            # abutements
            if inventory.abutements_type != 'none':
                elements.append(make_element('Abutment', 1, 'm2',
                                             deck_width * freeboard))
                elements.append(make_element('Abutment', 2, 'm2',
                                             deck_width * freeboard))

            # abutement cap total_deck_width
            if inventory.abutement_cap != 'none':
                elements.append(make_element(
                    'Abutment cap', 1, 'm', deck_width))
                elements.append(make_element(
                    'Abutment cap', 2, 'm', deck_width))

            # wing walls
            elements.append(make_element('Wing wall', 1, 'm2'))
            elements.append(make_element('Wing wall', 2, 'm2'))
            elements.append(make_element('Wing wall', 3, 'm2'))
            elements.append(make_element('Wing wall', 4, 'm2'))

            # pier(s)
            if inventory.pier != 'none':
                for i in range(1, bridge.num_of_spans):
                    elements.append(make_element('Pier', i, 'm2', None))

            # pier(s) cap
            if inventory.pier_cap != 'none':
                for i in range(1, bridge.num_of_spans):
                    elements.append(make_element(
                        'Pier cap', i, 'm', deck_width))

            # waterproofing
            for i in range(1, bridge.num_of_spans + 1):
                span_length = eval('bridge.span_length_' + str(i))
                elements.append(make_element('Waterproofing', i,
                                             'm', deck_width * span_length))

            # bearings
            for i in range(1, bridge.num_of_spans + 2):
                elements.append(make_element('Bearing', i, 'pcs'))

            # bearing seats
            if inventory.bearing_seat != 'no' and '':
                for i in range(1, bridge.num_of_spans + 2):
                    elements.append(make_element('Bearing seat', i, 'pcs'))

            # expansion joints
            for i in range(1, bridge.num_of_spans + 2):
                elements.append(make_element(
                    'Expansion joint', i, 'm', carriageway_width))

            # outlet pipe
            if inventory.outlet_pipes != 'none':
                elements.append(make_element('Outlet pipes', 1, 'pcs'))

            # Downsprout pipe
            if inventory.downsprout_pipes != 'none':
                elements.append(make_element('Downsprout pipe', 1, 'm'))

            # Deck drains !!! check if not None
            if inventory.deck_drains != 'none':
                elements.append(make_element('Deck drains', 1, 'pcs'))

            # Drain pipes !!! check if not None !!! WHAT???
            # if inventory.drain != 'none':
                # elements.append(make_element('Drain pipes', 1, 'pcs'))

            # Run-on slab !!! check if not None
            if inventory.run_on_slab_cover != 'none':
                elements.append(make_element('Run-on slab', 1, 'pcs'))
                elements.append(make_element('Run-on slab', 2, 'pcs'))

            # Run-on slab (kerbs) !!! check if not None
            if inventory.run_on_slab_kerbs != 'none':
                elements.append(make_element(
                    'Run-on slab (kerbs)', 1, 'm', 10))

            # Run-on slab (vehicle restraint system) !!! check if not None
            if inventory.run_on_slab_vehicle_restraint_system != 'none':
                elements.append(make_element(
                    'Run-on slab (vehicle restraint system)', 1, 'm', 10))

            # Run-on slab drainage gully !!! check if not None
            if inventory.run_on_slab_drainage_gully != 'none':
                elements.append(make_element(
                    'Run-on slab drainage gully', 1, 'm', 10))

            # Pavement/Overlay (road) !!! check if not None
            if inventory.pavement_road != 'none':
                elements.append(make_element(
                    'Pavement/Overlay (road)', 1, 'm2', carriageway_width * total_length))

            # Pavement/Overlay (sidewalk) !!! check if not None
            if inventory.pavement_sidewalk != 'none':
                elements.append(make_element(
                    'Pavement/Overlay (sidewalk)', 1, 'm2'))

            # Vehicle restraint system !!! check if not None
            if inventory.barrier_vehicle_restraint_system != 'none':
                elements.append(make_element(
                    'Vehicle restraint system', 1, 'm', total_length))

            # handrails !!! check if not None
            if inventory.barrier_hand_rails != 'none':
                elements.append(make_element(
                    'Handrails', 1, 'm', total_length))

            # signs !!! check if not None
            if inventory.bridge_sign is True:
                elements.append(make_element('Signs', 1, 'pcs'))

            # reflectors !!! check if not None
            if inventory.reflectors is True:
                elements.append(make_element('Reflectors', 1, 'pcs'))

            # Riverbed protection !!! check if not None
            if inventory.riverbed_protection != 'none':
                for i in range(1, bridge.num_of_spans + 1):
                    span_length = eval('bridge.span_length_' + str(i))
                    elements.append(make_element(
                        'Riverbed protection', i, 'm2', deck_width * span_length))

            # River training/slopes !!! check if not None
            if inventory.riverbed_slopes != 'none':
                elements.append(make_element('River training/slopes', 1, 'm2'))

            return elements
        else:
            api.abort(404, "No bridge found")


@bdc.route('/<string:bridge_id>/condition')
class Condition(Resource):
    @bdc.marshal_with(condition_model, envelope='condition')
    @bdc.response(404, 'No bridge found.')
    def get(self, bridge_id):
        """Get previous condition inspections with bridge ID.

        Allows to get a list of elements previously assessed in an inspection."""
        bridge = Bridge.query.filter_by(bridge_id=bridge_id).first()
        if bridge:
            insp = ConditionInspection.query.filter_by(
                bridge_id=bridge_id).all()
            return insp
        else:
            api.abort(404, "No bridge found")

    @bdc.expect([condition_model])
    @bdc.response(201, 'Inspection successfully added.')
    def post(self, bridge_id):
        """ Add bridge condition.

        Allows to submit bridge inventory inspection.
        The first 2 fields are mandatory.
        Pay attention to types."""
        for item in api.payload:
            inspection_date = datetime.strptime(item['inspection_date'],
                                                '%Y-%m-%dT%H:%M:%S.%fZ')
            item['inspection_date'] = inspection_date
            item['created_at'] = datetime.now()
            item['bridge_id'] = bridge_id
            new_condition = ConditionInspection(**item)
            db.session.add(new_condition)
        db.session.commit()

        return {'message': 'Inspection successfully added.'}, 201

        # TODO: add checking for bridge


@rms.route('/kpi/iri_roadclass_paved')
class IriRoadclassPaved(Resource):
    @rms.marshal_with(iri_roadclass_paved_model, envelope='kpi')
    def get(self):
        query = db.session.execute(iri_roadclass_paved)
        result = [dict(row.items()) for row in query.fetchall()]
        return result


@rms.route('/kpi/iri_roadclass_unpaved')
class IriRoadclassUnpaved(Resource):
    @rms.marshal_with(iri_roadclass_unpaved_model, envelope='kpi')
    def get(self):
        query = db.session.execute(iri_roadclass_unpaved)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/kpi/iri_zone_paved')
class IriZonePaved(Resource):
    @rms.marshal_with(iri_zone_paved_model, envelope='kpi')
    def get(self):
        query = db.session.execute(iri_zone_paved)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/kpi/iri_zone_unpaved')
class IriZoneUnpaved(Resource):
    @rms.marshal_with(iri_zone_unpaved_model, envelope='kpi')
    def get(self):
        query = db.session.execute(iri_zone_unpaved)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/kpi/iri_zoneroadclass_paved')
class IriZoneRoadclassPaved(Resource):
    @rms.marshal_with(iri_zoneroadclass_paved_model, envelope='kpi')
    def get(self):
        query = db.session.execute(iri_zoneroadclass_paved)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/kpi/iri_zoneroadclass_unpaved')
class IriZoneRoadclassUnpaved(Resource):
    @rms.marshal_with(iri_zoneroadclass_unpaved_model, envelope='kpi')
    def get(self):
        query = db.session.execute(iri_zoneroadclass_unpaved)
        result = [dict(row.items()) for row in query.fetchall()]
        return result


@rms.route('/kpi/pavement_condition_roadclass')
class PavementConditionRoadlcass(Resource):
    @rms.marshal_with(pavement_condition_roadclass_model, envelope='kpi')
    def get(self):
        query = db.session.execute(pavement_condition_roadclass)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/kpi/pavement_condition_zone')
class PavementConditionZone(Resource):
    @rms.marshal_with(pavement_condition_zone_model, envelope='kpi')
    def get(self):
        query = db.session.execute(pavement_condition_zone)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/kpi/pavement_condition_zoneroadclass')
class PavementConditionZoneRoadclass(Resource):
    @rms.marshal_with(pavement_condition_zoneroadclass_model, envelope='kpi')
    def get(self):
        query = db.session.execute(pavement_condition_zoneroadclass)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

# @rms.route('/reports/cracking')
# class Cracking(Resource):
#     @rms.marshal_with(cracking_model, envelope='report')
#     def get(self):
#         query = db.session.execute(cracking)
#         result = [dict(row.items()) for row in query.fetchall()]
#         return result

# @rms.route('/reports/edge_break')
# class EdgeBreak(Resource):
#     @rms.marshal_with(edge_break_model, envelope='report')
#     def get(self):
#         query = db.session.execute(edge_break)
#         result = [dict(row.items()) for row in query.fetchall()]
#         return result

# @rms.route('/reports/pavement_loss')
# class PavementLoss(Resource):
#     @rms.marshal_with(pavement_loss_model, envelope='report')
#     def get(self):
#         query = db.session.execute(pavement_loss)
#         result = [dict(row.items()) for row in query.fetchall()]
#         return result

# @rms.route('/reports/potholes')
# class Potholes(Resource):
#     @rms.marshal_with(potholes_model, envelope='report')
#     def get(self):
#         query = db.session.execute(potholes)
#         result = [dict(row.items()) for row in query.fetchall()]
#         return result

# @rms.route('/reports/roadwise_pdi')
# class RoadwisePDI(Resource):
#     @rms.marshal_with(roadwise_pdi_model, envelope='report')
#     def get(self):
#         query = db.session.execute(roadwise_pdi)
#         result = [dict(row.items()) for row in query.fetchall()]
#         return result

@rms.route('/reports/road_value')
class RoadValue(Resource):
    @rms.marshal_with(road_value_model, envelope='report')
    def get(self):
        query = db.session.execute(road_value)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/reports/roads')
class ReportRoad(Resource):
    @rms.marshal_with(report_roads_model, envelope='report')
    def get(self):
        query = db.session.execute(report_roads)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/reports/iri')
class ReportIRI(Resource):
    @rms.marshal_with(report_iri_model, envelope='report')
    def get(self):
        query = db.session.execute(report_iri)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/reports/traffic')
class ReportTraffic(Resource):
    @rms.marshal_with(report_traffic_model, envelope='report')
    def get(self):
        query = db.session.execute(report_traffic)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/reports/pdi')
class ReportPDI(Resource):
    @rms.marshal_with(report_pdi_model, envelope='report')
    def get(self):
        query = db.session.execute(report_pdi)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/reports/surface_type')
class ReportSurfaceType(Resource):
    @rms.marshal_with(report_surface_type_model, envelope='report')
    def get(self):
        query = db.session.execute(report_surface_type)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@rms.route('/webgis/iri/<int:zoom>/<string:min_lat>/<string:max_lat>/<string:min_lon>/<string:max_lon>')
class WebGISIRI(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self, zoom, min_lat, max_lat, min_lon, max_lon):
        if zoom < 13:
            zoom = 10
        elif 13 <= zoom < 16:
            zoom = 13
        elif 16 <= zoom:
            zoom = 16

        sql = webgis_iri.format(min_lat, max_lat, min_lon, max_lon, zoom)
        df = pd.read_sql(sql, con=db.engine)
        roads = list(df.roadcode.unique())

        result = []
        for road in roads:
            result.append({road: df[df.roadcode == road].drop(columns=['roadcode']).to_dict('records')})
        return result

@rms.route('/webgis/iri/roads')
class WebGISIRIRoads(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self):
        df = pd.read_sql(webgis_iri_roads, con=db.engine)
        roads = list(df.roadcode.unique())

        result = []
        for road in roads:
            result.append({road: df[df.roadcode == road].drop(columns=['roadcode']).to_dict('records')})
        return {'roads': result}

@rms.route('/webgis/iri/roads/<string:roadcode>')
class WebGISIRIRoadData(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self, roadcode):
        # TODO: check if road exists
        sql = webgis_iri_road_data.format(roadcode)
        print(sql)
        df = pd.read_sql(sql, con=db.engine)
        return {'road_data': df.to_dict('records')}

@rms.route('/webgis/traffic/roads')
class WebGISTrafficRoads(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self):
        df = pd.read_sql(webgis_traffic_roads, con=db.engine)
        roads = list(df.roadcode.unique())

        result = []
        for road in roads:
            result.append({road: df[df.roadcode == road].drop(columns=['roadcode']).to_dict('records')})
        return {'roads': result}

@rms.route('/webgis/traffic/roads/<string:roadcode>')
class WebGISTrafficRoadData(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self, roadcode):
        # TODO: check if road exists
        sql = webgis_traffic_road_data.format(roadcode)
        df = pd.read_sql(sql, con=db.engine)
        return {'road_data': df.to_dict('records')}

@rms.route('/webgis/roads/<int:zoom>/<string:min_lat>/<string:max_lat>/<string:min_lon>/<string:max_lon>')
class WebGISRoads(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self, zoom, min_lat, max_lat, min_lon, max_lon):
        if zoom < 13:
            zoom = 10
        elif 13 <= zoom < 16:
            zoom = 13
        elif 16 <= zoom:
            zoom = 16

        sql = webgis_roads.format(min_lat, max_lat, min_lon, max_lon, zoom)
        df = pd.read_sql(sql, con=db.engine)
        roads = list(df.roadcode.unique())

        result = []
        for road in roads:
            result.append({road: df[df.roadcode == road].drop(columns=['roadcode']).to_dict('records')})
        return result

@rms.route('/webgis/traffic/<int:zoom>/<string:min_lat>/<string:max_lat>/<string:min_lon>/<string:max_lon>')
class WebGISTraffic(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self, zoom, min_lat, max_lat, min_lon, max_lon):
        if zoom < 13:
            zoom = 10
        elif 13 <= zoom < 16:
            zoom = 13
        elif 16 <= zoom:
            zoom = 16

        sql = webgis_traffic.format(min_lat, max_lat, min_lon, max_lon, zoom)
        df = pd.read_sql(sql, con=db.engine)
        roads = list(df.roadcode.unique())

        result = []
        for road in roads:
            result.append({road: df[df.roadcode == road].drop(columns=['roadcode']).to_dict('records')})
        return result

@rms.route('/dashboard')
class RMSDashboard(Resource):
    @rms.marshal_with(dashboard_model, envelope='dashboard-items')
    def get(self):
        result = [
            {
                'Value': str(db.session.execute(dashboard_value_abc).fetchall()[0][0]) + 'M',
                'Text': 'Value of ABC roads',
                'Sub-text': 'In Million ECD',
                'Icon': 'fas fa-money-bill-wave'
            },
            {
                'Value': db.session.execute(dashboard_number_abc).fetchall()[0][0],
                'Text': 'Number of ABC roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-road'
            },
            {
                'Value': db.session.execute(dashboard_length_abc).fetchall()[0][0],
                'Text': 'Length of ABC roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-ruler'
            },
            {
                'Value': db.session.execute(dashboard_iri_abc).fetchall()[0][0],
                'Text': 'Avg IRI of ABC roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-chart-line'
            },
            {
                'Value': db.session.execute(dashboard_number_a).fetchall()[0][0],
                'Text': 'Number of A roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-road'
            },
            {
                'Value': db.session.execute(dashboard_number_b).fetchall()[0][0],
                'Text': 'Number of B roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-road'
            },
            {
                'Value': db.session.execute(dashboard_number_c).fetchall()[0][0],
                'Text': 'Number of C roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-road'
            },
            {
                'Value': db.session.execute(dashboard_length_a).fetchall()[0][0],
                'Text': 'Length of A roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-ruler'
            },
            {
                'Value': db.session.execute(dashboard_length_b).fetchall()[0][0],
                'Text': 'Length of B roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-ruler'
            },
            {
                'Value': db.session.execute(dashboard_length_c).fetchall()[0][0],
                'Text': 'Length of C roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-ruler'
            },
            {
                'Value': db.session.execute(dashboard_aadt_a).fetchall()[0][0],
                'Text': 'AADT of A roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-truck-pickup'
            },
            {
                'Value': db.session.execute(dashboard_aadt_b).fetchall()[0][0],
                'Text': 'AADT of B roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-truck-pickup'
            },
            {
                'Value': db.session.execute(dashboard_aadt_c).fetchall()[0][0],
                'Text': 'AADT of C roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-truck-pickup'
            },
            {
                'Value': db.session.execute(dashboard_iri_a).fetchall()[0][0],
                'Text': 'Avg IRI of A roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-chart-line'
            },
            {
                'Value': db.session.execute(dashboard_iri_b).fetchall()[0][0],
                'Text': 'Avg IRI of B roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-chart-line'
            },
            {
                'Value': db.session.execute(dashboard_iri_c).fetchall()[0][0],
                'Text': 'Avg IRI of C roads',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-chart-line'
            },
        ]
        return result
@bms.route('/dashboard')
class BMSDashboard(Resource):
    @rms.marshal_with(dashboard_model, envelope='dashboard-items')
    def get(self):
        result = [
            {
                'Value': str(db.session.execute(bms_dashboard_bridges).fetchall()[0][0]),
                'Text': 'Number of Bridges',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-archway'
            },
            {
                'Value': str(db.session.execute(bms_dashboard_value).fetchall()[0][0]),
                'Text': 'Value of Bridges',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-money-bill-wave'
            },
            {
                'Value': str(db.session.execute(bms_dashboard_average_bci).fetchall()[0][0]),
                'Text': 'Average BCI',
                'Sub-text': '_' * 20,
                'Icon': 'fas fa-wrench'
            },
        ]
        return result


@bms.route('/webgis')
class BMSWebGIS(Resource):
    @bms.marshal_with(bms_webgis_model, envelope='bridges')
    def get(self):
        query = db.session.execute(bms_webgis)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@bms.route('/kpi/condition')
class BMSKPICondition(Resource):
    @bms.marshal_with(bms_kpi_condition_model, envelope='kpi')
    def get(self):
        query = db.session.execute(bms_kpi_condition)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@bms.route('/report/value')
class BMSReportValue(Resource):
    @bms.marshal_with(bms_report_value_model, envelope='report')
    def get(self):
        query = db.session.execute(bms_report_value)
        result = [dict(row.items()) for row in query.fetchall()]
        return result

@fwd.route('/points/<string:min_lat>/<string:max_lat>/<string:min_lon>/<string:max_lon>')
class FWDPoints(Resource):
    # @rms.marshal_with(webgis_iri_model, envelope='report')
    def get(self, min_lat, max_lat, min_lon, max_lon):
        sql = fwd_points.format(min_lat, max_lat, min_lon, max_lon)
        df = pd.read_sql(sql, con=db.engine)
        result = {'points': df.to_dict('records')}
        return result
